# Off-Path Attacking and Challenge-Response Auth
2021/07/10 7.10 P.M

![blind-attacker](blind-attacker1.jpg)

```“Blind attacker is on action. Off-path adversary, also called spoofer and blind attacker.”```

In computer network security, attackers are classified according to their capabilities. These groups are known as attack models or adversary models. Eavesdropping adversary model, MitM adversary model, Off-path adversary model are few examples for them. <br>
The off-path adversary is a well-known model we can find. The off-path attacker is often referred to by other names, including spoofer, spoofing or blind.

***Keywords: adversary models, Eavesdropping, MitM, Off-path adversary, spoofing*** 
## MitM Adversary Model
The term 'MitM' refers to 'Man-in-the-Middle' or 'Monster-in-the-Middle'. A MitM attacker is located middle the path between communicating parties, and can manipulate the 
communication between them in any way (intercept, modify, block and inject spoofed packets). The attacker intercepts ongoing communication without knowing to both endpoints. Monster-in-the-Middle adversary, also called 'on-path attacker'.
## Off-Path Adversary (Attacker) Model
Unlike a MitM attacker, an off-path attacker cannot observe or modify legitimate packets sent between other parties. This attacker can transmit packets with a spoofed (fake) source IP address - impersonating some legitimate party. In other words off-path adversary mislead recipients to think the message was sent by somebody else which is sent by him self. For an example, as illustrated Figure below Oscar, the off-path attacker, sends a message to Bob, specifying Alice’s name as the sender.
<br>

![off-path-attacker-spoofed call](Off-path-attacker.png)
<br>

The off-path attacker is significantly weaker than the MitM attacker. In particular, it cannot eavesdrop or modify messages sent by other parties. Below Figure shows the behaviour of both adversaries. 

![network-model-mitm-and-off-path](network-model.png)

## Off-Path Adversary used to..
Looking at the history of off-path attacks, many of them were used for phishing information. Off-path attacker succeeded for malicious TCP injection, hijack a TCP connection, DNS poisoning. For an instance, with the help of unprivileged malware, a successful adversary can hijack an HTTP session and return a phishing Facebook login page issued by a browser. <br>
The time line of off-path-attacks begin with 1985.

## Spoofing
Act of disguising a communication from an unknown source as being from a known trusted source is called as 'Spoofing'. Spoofing is a way bad actor gain access in order to execute a cyber attack.

## Challenge - response defenses
The key to the off-path attacks is challenge-response defenses. Challenge-response defenses are often relied upon to distinguish between spoofed packets from an off-path attacker and legitimate packets from legitimate(verified) communication end point. In order to authenticate a response from a server, a client sends a random challenge with the request, which is echoed in the response.

Since an off-path attacker(Oscar), cannot eavesdrop on packets exchanged between the server and the client, it appears that Oscar would have to guess the  challenge. Therefore, the challenge is to avoid creating an Oscar packet with a valid response. If Oscar able to guess the valid response, we call it as challenge-response defense has failed against the attacker.

The security of most Internet applications, e.g., email, web surfing, and most peer-to-peer applications, relies on challenge-response mechanisms.

### Authors
<img src="../shared/authors/kavindu.jpg" style="border-radius: 30px 30px 0px 30px;" alt="drawing" width="50"/>
Kavindu Gunathilake