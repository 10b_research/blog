# Overview of Intrusion Detection and Prevention System
2021/07/03 08.27 P.M

## What is IDS and IPS ?

An **intrusion detection system (IDS)** is a device or software application that monitors a network for malicious activity or policy violations. Any malicious venture or violation is normally reported either to an administrator or collected centrally using a security information and event management (SIEM) system.

An **intrusion prevention system (IPS)** is also an IDS which are capable of responding to detected intrusion upon discovery.
<br><br>

<img src="ids-vs-ips.png" alt="ids-vs-ips" width="500"/>

## Common functionalities  

Both systems can,
- **Monitor.** After setup, these programs can look over traffic within parameters you specify, and they will work until you turn them off.  

- **Alert.** Both programs will send a notification to those you specify when a problem has been spotted.

- **Learn.** Both can use machine learning to understand patterns and emerging threats.

- **Log.** Both will keep records of attacks and responses, so you can adjust your protections accordingly.

## Difference between them  

- IDS main objective is to analyze and detect any vulnerabilities of inline traffic streams of a network. Usually, IDS uses TAP or SPAN ports to analyze the traffic. 

    - **TAP (Test Access Point)**  
        TAP is a device that place under two devices and data will be flow through the TAP and it will create the copy of data for monitoring where the IDS gets data and original data will flow through the network.
        <br> 

        <img src="tap.PNG" alt="tap" width="450"/>
        
        <table>
        <tr>
        <td>
            <img src="tap_device.png" alt="tap" width="80"/>
        </td>
        <td>Example of a TAP (KUNBUS-TAP 2100)</td>
        </tr>
        </table>
        
    - **SPAN**  
        SPAN port is also known as Mirror port not like the TAP port; this doesn’t have to be a separate physical device that can be built into the switch or the router. Can selects specify packets transferring through the device and send copy of them to SPAN Port. 
        <br> 

        <img src="span.png" alt="span" width="450"/>
        <br><br>  

    Once the packets are monitored through that received from either TAP or SPAN, they will be monitored and the techniques that are used to monitor are discussed in the [last article](../week1/post.md). IDS are placed as out of band because they are not indent to inspect every packet that traverse the network. IDS main goal is to detect and monitor the threats but unlike IDS, IPS is placed inline directly between the source and the destination. Functionalities that IPS can do over IDS are,  
    - Dropping suspicious or malicious packets before reaching destination.
    - Resetting Connection

## Widely used IPS and IDS applications  

- GigaSMART 
- Cisco IPS
- DarkTrace  
- NGIPS 
 
When we go through any of the modern IPS and IDS any of them not act only as an IPS or IDS major of them are following a hybrid approach to Identify and Prevent.  

### References and further reading,
- [Intrusion Prevention System (IPS) In-depth Analysis](https://gbhackers.com/intrusion-prevention-systemips-and-its-detailed-funtion-socsiem/)
- [IPS and IDS: Role and Function](https://blogs.keysight.com/blogs/tech/traf-gen.entry.html/2020/04/01/ips_and_ids_rolean-IdyO.html)
- [IDS vs. IPS: Definitions, Comparisons & Why You Need Both](https://www.okta.com/identity-101/ids-vs-ips/)

### Authors
<img src="../shared/authors/infaz.png" style="border-radius: 30px 30px 0px 30px;" alt="drawing" width="50"/>
Infaz Rumy
<img src="../shared/authors/sadesh.jpg" style="border-radius: 30px 30px 0px 30px;" alt="drawing" width="65"/>
Sadesh Surendra

