# What is an Intrusion Detection System?
2021/06/26 10.40 P.M

![header-image](main.jpg)
<br>
```“Most computer security experts believe that the vast majority of computer crimes are committed by an organization’s authorized users, its insiders.”```

An IDS (Intrusion Detection System) is a device or application used to inspect all network traffic and alert the user or administrator when there has been an unauthorized attempt or access. The two primary methods of monitoring are signature-based and anomaly-based. 

As every application software,IDS also can be separated based on the environment they are operating in.
    
    What are zero-day threats?
        Zero-day threats are the threats which are not identified yet.

    What are false positive / false negative detections?
        False positive detections mean the incorrect detections.
        False negative detections mean the missed detections

![hids-nids](nids-hids.jpg)
## Host-based (HIDS)

The host based IDS applications deploy to a client device and protect it against  both internal and external threats.HIDS have deep access to the host computer's/ device's internal system.Therefore it can monitor the network traffic originated from the host device and the network traffic which is received.

## Network-based (NIDS)
The network based IDSs are designed to inspect the flowing network traffic  through a certain network. So the IDS can make decisions by overseeing the metadata and the content of the network activities. NIDS have the ability to cover more context than HIDS, Also the NIDS do not have deep access to the endpoint internal system.

However, network security experts say that using only HIDS or NIDS will not help to protect your network completely. To achieve a  comprehensive security, you might need to implement several security solutions at different places of the network.
## Mechanisms
When it comes to the threat detection mechanisms used in IDS, they can be categorized into two main mechanisms.

### 1. Signature based detections
These types of IDS maintain a list of known malicious threats previously detected. Based on the known threat signatures, the IDS will observe the network activity and generate the alerts. As all the alerts are generated based on the known signature, the accuracy is high.But these types of IDS cannot detect the zero-day threats.
 		
### 2. Anomaly based detection
Usually people categorize (good/ bad) others by looking at their behaviour. Likewise anomaly based IDS solutions create a model of the normal behaviours of the network and use it to compare and recognize the future behaviour. Then the system can detect anomalies within the network. Anomaly based IDS have an ability to detect zero-day threats, which Signature based IDS could not. But the system needs to balance the false positive and false negative detections.

## The most asked question, Why do we need an IDS?

No firewall is foolproof, no network is impenetrable. People continuously find ways to trespass security of networks and firewalls. Most attackers get access to the systems with stolen credentials using social engineering methods or malware. Even Though someone inside has access like that, having an inner protector to protect your network is a plus point. That is why we need an IDS.

Ensuring safe and trusted communication of information in the network is the main goal of an Intrusion Detection System. This is the first of many articles. We are hoping to introduce many more details about IDS and its background in future.

### Authors
<img src="../shared/authors/nadun.jpg" style="border-radius: 30px 30px 0px 30px;" alt="drawing" width="50"/>
Nadun Yashmika
<img src="../shared/authors/seniya.jpg" style="border-radius: 30px 30px 0px 30px;" alt="drawing" width="50"/>
Seniya Sansith
<img src="../shared/authors/dharana.jpeg" style="border-radius: 30px 30px 0px 30px;" alt="drawing" width="50"/>
Dharana Rodrigo

